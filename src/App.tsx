import './App.css';
import Head, { HeadProps } from './Components/Head/Head';
import MainContent from './Components/MainContent/MainContent';
import Skill, { SkillProps } from './Components/Skill/Skill';
import SkillsBlock from './Components/SkillsBlock/SkillsBlock';
import image from './assets/image.png';

import pythonIcon from './assets/python.svg';
import linuxIcon from './assets/linux.svg';
import gitIcon from './assets/git.svg';
import dockerIcon from './assets/docker.svg';

import tsIcon from './assets/ts.svg';
import jsIcon from './assets/js.svg';
import k8sIcon from './assets/kubernetes-svgrepo-com.svg';
import redisIcon from './assets/redis.svg';
import reactIcon from './assets/react.svg';
import graphQLIcon from './assets/graphQL.svg';
import postgreIcon from './assets/postgresql.svg';
import nginxIcon from './assets/nginx.svg';
import rabbitMQIcon from './assets/rabbitmq.svg';
import cssIcon from './assets/css.svg';

function App() {
  const headProps: HeadProps = {
    name: 'Никитенко Иван Юрьевич',
    imageSrc: image,
  };

  const skills: Array<SkillProps> = [
    { title: 'Python', iconSrc: pythonIcon },
    { title: 'Linux', iconSrc: linuxIcon },
    { title: 'Docker', iconSrc: dockerIcon },
    { title: 'Git', iconSrc: gitIcon },
  ];

  const skillsForLearn: Array<SkillProps> = [
    { title: 'Kubernetes', iconSrc: k8sIcon },
    { title: 'TypeScript', iconSrc: tsIcon },
    { title: 'JavaScript', iconSrc: jsIcon },
    { title: 'RabbitMQ', iconSrc: rabbitMQIcon },
    { title: 'Redis', iconSrc: redisIcon },
    { title: 'React.js', iconSrc: reactIcon },
    { title: 'GraphQL', iconSrc: graphQLIcon },
    { title: 'Nginx', iconSrc: nginxIcon },
    { title: 'PostgreSQL', iconSrc: postgreIcon },
    { title: 'CSS', iconSrc: cssIcon },
  ];

  return (
    <MainContent>
      <Head {...headProps} />
      <SkillsBlock title="Компетенции">
        {skills.map((item, _id) => (
          <Skill key={_id} title={item.title} iconSrc={item.iconSrc} />
        ))}
      </SkillsBlock>
      <SkillsBlock title="Компетенции к изучению">
        {skillsForLearn.map((item, _id) => (
          <Skill key={_id} title={item.title} iconSrc={item.iconSrc} />
        ))}
      </SkillsBlock>
    </MainContent>
  );
}

export default App;
