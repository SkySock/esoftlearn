import { ReactElement } from 'react';
import classes from './SkillsBlock.module.css';

interface SkillsBlockProps {
  title?: string;
  children: Array<ReactElement> | ReactElement;
}

function SkillsBlock(props: SkillsBlockProps) {
  const title = props.title ? <h3>{props.title}</h3> : <></>;
  return (
    <>
      <div>
        {title}
        <div className={classes.block}>{props.children}</div>
      </div>
    </>
  );
}

export default SkillsBlock;
