import { ReactElement } from 'react';
import classes from './MainContent.module.css';

interface MainContentProps {
  children: Array<ReactElement> | ReactElement;
}

function MainContent(props: MainContentProps) {
  return <div className={classes.content}>{props.children}</div>;
}

export default MainContent;
