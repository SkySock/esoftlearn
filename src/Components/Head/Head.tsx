import Avatar from '../Avatar/Avatar';
import classes from './Head.module.css';

export interface HeadProps {
  name: string;
  imageSrc: string;
}

function Head(props: HeadProps) {
  return (
    <div className={classes.head}>
      <Avatar src={props.imageSrc} />
      <h1>{props.name}</h1>
    </div>
  );
}

export default Head;
