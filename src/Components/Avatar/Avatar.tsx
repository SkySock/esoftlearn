import classes from './Avatar.module.css';

interface AvatarProps {
  src: string;
}

function Avatar(props: AvatarProps) {
  return <img className={classes.image} src={props.src} />;
}

export default Avatar;
