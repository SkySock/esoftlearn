import classes from './Skill.module.css';

export interface SkillProps {
  title: string;
  iconSrc: string;
}

function Skill(props: SkillProps) {
  return (
    <>
      <div className={classes.skill}>
        <img className={classes.icon} src={props.iconSrc} width="80px" />
        <p>{props.title}</p>
      </div>
    </>
  );
}

export default Skill;
